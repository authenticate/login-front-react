import React, { Component } from 'react';
import '../styles/Navbar.css';
// import logo from '../assets/images/logo.svg';
import PropTypes from 'prop-types';
// import Link from 'react-router-dom/Link';
// import { Button } from 'reactstrap';
import { Link } from 'react-router-dom';

class Navbar extends Component {
    static propTypes = {
        title: PropTypes.string.isRequired,
        routes: PropTypes.array.isRequired
    };
    render() {
        const { title, routes } = this.props;
        return (
            <nav className="navbar-expand-lg navbar navbar-dark bg-dark">
                <div className="navbar-brand">
                    {title}
                </div>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item active">
                            <div className="nav-link">
                                <Link to="/">{routes[0].title}</Link>
                            </div>
                        </li>
                        <li className="nav-item">
                            <div className="nav-link">
                                <Link to="/view">{routes[1].title}</Link>
                            </div>
                        </li>
                        <li className="nav-item">
                            <div className="nav-link">
                                <Link to='/information'>{routes[2].title}</Link>
                            </div>
                        </li>
                    </ul>
                    <form className="form-inline my-2 my-lg-0">
                        <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
                        <button className="btn btn-outline-success my-2 my-sm-0" >Search</button>
                    </form>
                </div>
            </nav>

            // <div className='Navbar'>
            //     <div className='Logo'>
            //         <img src={logo} className='Logo' alt='logo' />
            //         <h1 className='Navbar-title'>Welcome to React</h1>
            //         <h2>
            //             {title}
            //         </h2>
            //         <ul className='Menu'>
            //             {
            //                 routes && routes.map(
            //                     (item, key) => <li key={key}><Link to={item.url}> {item.title} </Link></li>
            //                 )
            //             }
            //         </ul>
            //     </div>
            // </div>
        );
    }
}

export default Navbar;