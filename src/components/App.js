import React, { Component } from 'react';
import '../styles/App.css';
import PropTypes from 'prop-types';

import Navbar from '../components/Navbar';
import routes from '../Routes/routes';
import Content from '../components/Content';

class App extends Component {
  static propTypes = {
    children: PropTypes.object.isRequired
  };
  render() {
    const { children } = this.props;
    return (
      <div className="App">
        <Navbar title="Navbar" routes={routes}></Navbar>
        <Content body={children}></Content>
      </div>
    );
  }
}

export default App;
