export default [
    {
        title: 'Home',
        url: '/'
    },
    {
        title: 'View',
        url: '/view'
    },
    {
        title: 'Information',
        url: '/information'
    },
]