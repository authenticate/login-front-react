import React from 'react';
import { Route, Switch } from 'react-router-dom';

// add components
import App from '../components/App';
import Home from '../components/Home';
import View from '../components/View';
import Information from '../components/Information';
import Page404 from '../components/Page404';

const AppRoutes = () =>
    <App>
        <Switch>
            <Route exact path="/view" component={View} />
            <Route exact path="/information" component={Information} />
            <Route exact path="/" component={Home} />
            <Route component={Page404}/>
        </Switch>
    </App>;


export default AppRoutes;